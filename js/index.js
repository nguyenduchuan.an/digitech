$(document).ready(function() {
    $('.toogle-search').hover(function(e) {
        // e.preventDefault();
        $('.toogle-search').css('visibility', 'hidden');
        $(this).next().addClass('show');
    }, function() {

    });
    $("body").click(function(e) {
        // e.stopPropagation();
        $('.search-sticky').removeClass('show');
        $('.toogle-search').css('visibility', 'visible');
    });
    $(".search-sticky").click(function(e) {
        e.stopPropagation();
    });
    $('.btn-showpass').click(function(e) {
        $(this).toggleClass('show');
        if ($('.form-group').hasClass('show')) {
            $('.input-pass').attr('type', 'text');
        }
    });

    $('.heart-pro').click(function(e) {
        e.preventDefault();
        $(this).toggleClass('active');
    });
    $('.show-login').click(function(e) {
        e.preventDefault();
        $(".sub-dropdown").slideToggle();
    })
    

    /** Menu mobile **/
    $(".accodition").click(function() {
        $(this).next().slideToggle();
        $(this).toggleClass('rotate');
    });
    $(".open-sidemenu").click(function() {
        $('#sidenav').toggleClass('menu-mobile');
        $('.block-overlay').toggleClass('over');
        $('body').toggleClass('no-scroll');
        $('.block-header-sale').addClass('slow-layer');
    });
    $(".block-overlay").click(function() {
        $('#sidenav').toggleClass('menu-mobile');
        $(this).toggleClass('over');
        $('body').toggleClass('no-scroll');
        $('.block-header-sale').removeClass('slow-layer');
    });
    /** Menu mobile **/

    $(function() {
        var star = '.star',
            selected = '.selected';

        $(star).on('click', function() {
            $(selected).each(function() {
                $(this).removeClass('selected');
            });
            $(this).addClass('selected');
        });

    });

    $(".block-category .ttl-category").click(function(e) {
        $(".list-category").slideToggle();
        e.stopPropagation();
    });

    $(".block-category .list-category").click(function(e) {
        e.stopPropagation();
    });

    $("html").click(function() {
        $(".list-category").slideUp();
        $(".search-mobile .form-group").removeClass("on");
    });

    //click show search mobile 
    $(".show-search-mb").click(function(e) {
        e.stopPropagation();
        $(".search-mobile .form-group").toggleClass("on");
    });
    $(".search-mobile .form-group").click(function(e) {
        e.stopPropagation();
    });
});

$('.owl-banner').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    autoplay: true,
    nav: true,
    dots: true,
    dotsSpeed: 1000,
    autoplaySpeed: 1000,
    navSpeed: 800,
    responsive: {
        0: {
            items: 1
        }
    }
});

$('.owl-product-02').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    // autoplay: true,
    nav: true,
    dots: true,
    dotsSpeed: 1000,
    autoplaySpeed: 2000,
    navSpeed: 800,
    autoplayTimeout: 6000,
    responsive: {
        0: {
            items: 1
        }
    }
});
$('.owl-product').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    // autoplay: true,
    nav: true,
    dots: true,
    dotsSpeed: 1000,
    autoplaySpeed: 2000,
    navSpeed: 800,
    autoplayTimeout: 4000,
    responsive: {
        0: {
            items: 1
        }
    }
});
$('.owl-product-03').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    // autoplay: true,
    nav: true,
    dots: true,
    dotsSpeed: 1000,
    autoplaySpeed: 2000,
    navSpeed: 800,
    autoplayTimeout: 4000,
    responsive: {
        0: {
            items: 1
        }
    }
});

// menu
(function($) {
    'use strict';

    var defaults = {
        topOffset: 400, //px - offset to switch of fixed position
        hideDuration: 300, //ms
        stickyClass: 'is-fixed'
    };

    $.fn.stickyPanel = function(options) {
        if (this.length == 0) return this; // returns the current jQuery object

        var self = this,
            settings,
            isFixed = false, //state of panel
            stickyClass,
            animation = {
                normal: self.css('animationDuration'), //show duration
                reverse: '', //hide duration
                getStyle: function(type) {
                    return {
                        animationDirection: type,
                        animationDuration: this[type]
                    };
                }
            };

        // Init carousel
        function init() {
            settings = $.extend({}, defaults, options);
            animation.reverse = settings.hideDuration + 'ms';
            stickyClass = settings.stickyClass;
            $(window).on('scroll', onScroll).trigger('scroll');
        }

        // On scroll
        function onScroll() {
            if (window.pageYOffset > settings.topOffset) {
                if (!isFixed) {
                    isFixed = true;
                    self.addClass(stickyClass)
                        .css(animation.getStyle('normal'));
                }
            } else {
                if (isFixed) {
                    isFixed = false;

                    self.removeClass(stickyClass)
                        .each(function(index, e) {
                            // restart animation
                            // https://css-tricks.com/restart-css-animation/
                            void e.offsetWidth;
                        })
                        .addClass(stickyClass)
                        .css(animation.getStyle('reverse'));

                    setTimeout(function() {
                        self.removeClass(stickyClass);
                    }, settings.hideDuration);
                }
            }
        }

        init();

        return this;
    }
})(jQuery);

$('.owl-partner').owlCarousel({
    loop: false,
    margin: 50,
    nav: true,
    autoplay: false,
    nav: true,
    navText: ["<img src='images/common/icon-prev.png'>", "<img src='images/common/icon-next.png'>"],
    dots: false,
    responsive: {
        0: {
            items: 2
        },
        600: {
            items: 3
        },
        1000: {
            items: 6
        }
    }
});

$('.owl-related').owlCarousel({
    loop: false,
    margin: 30,
    nav: true,
    autoplay: false,
    nav: true,
    dots: false,
    responsive: {
        0: {
            items: 1
        },
        480: {
            items: 2
        },
        900: {
            items: 3
        },
        1000: {
            items: 4
        }
    }
});

$(document).ready(function() {
    var x = $("#block-header-top").offset().top + 20;
    $(window).scroll(function() {
        if ($(this).scrollTop() > x) {
            $('#block-header').addClass("fixed");
        } else {
            $('#block-header').removeClass("fixed");
        }
    });
});

// slide price
$(function() {
    $("#slider-range").slider({
        range: true,
        min: 0,
        max: 7000000,
        values: [50000, 4000000],
        slide: function(event, ui) {
            $("#amount").val(ui.values[0] + "đ" + " - " + ui.values[1] + "đ");
        }
    });
    $("#amount").val($("#slider-range").slider("values", 0) + "đ" +
        " - " + $("#slider-range").slider("values", 1) + "đ");
});


// double owl
document.addEventListener("DOMContentLoaded", function() {
    console.log("start");
    let galleries = document.querySelectorAll(".gallery");

    Array.prototype.forEach.call(galleries, function(el, i) {
        const $this = $(el);
        const $owl1 = $this.find(".owl-1");
        const $owl2 = $this.find(".owl-2");
        let flag = false;
        let duration = 300;

        $owl1
            .owlCarousel({
                items: 1,
                lazyLoad: false,
                loop: false,
                dots: false,
                margin: 10,
                nav: false,
                responsiveClass: true
            })
            .on("changed.owl.carousel", function(e) {
                if (!flag) {
                    flag = true;
                    $owl2
                        .find(".owl-item")
                        .removeClass("current")
                        .eq(e.item.index)
                        .addClass("current");
                    if (
                        $owl2
                        .find(".owl-item")
                        .eq(e.item.index)
                        .hasClass("active")
                    ) {} else {
                        $owl2.trigger("to.owl.carousel", [e.item.index, duration, true]);
                    }
                    flag = false;
                }
            });

        $owl2
            .on("initialized.owl.carousel", function() {
                $owl2
                    .find(".owl-item")
                    .eq(0)
                    .addClass("current");
            })
            .owlCarousel({
                items: 4,
                lazyLoad: false,
                loop: false,
                margin: 10,
                nav: false,
                dots: false,
                responsiveClass: true
            })
            .on("click", ".owl-item", function(e) {
                e.preventDefault();
                var number = $(this).index();
                $owl1.trigger("to.owl.carousel", [number, duration, true]);
            });
    });
});

//Select tag
var x, i, j, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-selectag");
for (i = 0; i < x.length; i++) {
    selElmnt = x[i].getElementsByTagName("select")[0];
    /*for each element, create a new DIV that will act as the selected item:*/
    a = document.createElement("DIV");
    a.setAttribute("class", "select-selected");
    a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
    x[i].appendChild(a);
    /*for each element, create a new DIV that will contain the option list:*/
    b = document.createElement("DIV");
    b.setAttribute("class", "select-items select-hide");
    for (j = 1; j < selElmnt.length; j++) {
        /*for each option in the original select element,
        create a new DIV that will act as an option item:*/
        c = document.createElement("DIV");
        c.innerHTML = selElmnt.options[j].innerHTML;
        c.addEventListener("click", function (e) {
            /*when an item is clicked, update the original select box,
            and the selected item:*/
            var y, i, k, s, h;
            s = this.parentNode.parentNode.getElementsByTagName("select")[0];
            h = this.parentNode.previousSibling;
            for (i = 0; i < s.length; i++) {
                if (s.options[i].innerHTML == this.innerHTML) {
                    s.selectedIndex = i;
                    h.innerHTML = this.innerHTML;
                    y = this.parentNode.getElementsByClassName("same-as-selected");
                    for (k = 0; k < y.length; k++) {
                        y[k].removeAttribute("class");
                    }
                    this.setAttribute("class", "same-as-selected");
                    break;
                }
            }
            h.click();
        });
        b.appendChild(c);
    }
    x[i].appendChild(b);
    a.addEventListener("click", function (e) {
        /*when the select box is clicked, close any other select boxes,
        and open/close the current select box:*/
        e.stopPropagation();
        closeAllSelect(this);
        this.nextSibling.classList.toggle("select-hide");
        this.classList.toggle("select-arrow-active");
    });
}

function closeAllSelect(elmnt) {
    /*a function that will close all select boxes in the document,
    except the current select box:*/
    var x, y, i, arrNo = [];
    x = document.getElementsByClassName("select-items");
    y = document.getElementsByClassName("select-selected");
    for (i = 0; i < y.length; i++) {
        if (elmnt == y[i]) {
            arrNo.push(i)
        } else {
            y[i].classList.remove("select-arrow-active");
        }
    }
    for (i = 0; i < x.length; i++) {
        if (arrNo.indexOf(i)) {
            x[i].classList.add("select-hide");
        }
    }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);
$(document).ready(function () {
    $('.custom-selectag-2 .select-selected').empty();
    $('.select-selected').click(function (e) {
        e.preventDefault();

        if ($('.select-selected').is(':empty')) {
            $(this).prev().addClass('tranf');
        } else {

        }
    });

});
